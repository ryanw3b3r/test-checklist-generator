export default () => setTimeout(() => {
  document.querySelectorAll('code').forEach(
    (el) => {
      el.addEventListener('click', (ev) => {
        ev.preventDefault()
        ev.stopPropagation()
        navigator.clipboard
          .writeText(el.innerHTML)
          .then(() => {
            ev.target.classList.add('copied')
            setTimeout(() => ev.target.classList.remove('copied'), 3000)
          })
      })
  })
}, 1000)
