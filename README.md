# Test Doc Generator

Generates a list of steps to complete during testing.
Every time I deploy bigger features for a client I have to
test their whole website, so this tool allows me to
prepare steps in JSON file and it will generate checklist
that I can follow to test all features. Checklist can
change depending on environment - clicking on the website
URL at the top switches between environments.

To speed up things there's a auto-copy feature, links to
pages and for fun even hints on hover.

## Sample data

Sample data included in project are for PA Pool's website.
There are 3 servers, so 3 possible test scenarios: localhost,
staging and production.

## How to run?

- `npm install`
- `npm run dev`

## Author

Ryan Weber, One Smart Space Ltd. © 2023 All rights reserved.
